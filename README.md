# PetStore API Automation
## Introduction

This is a test solution for endpoints available in https://petstore.swagger.io/. It includes operations of 3 endpoints - Pet, Store and User.

The components of this framework are Serenity, RestAssured, Cucumber, JUnit & Maven.

## Project structure

```
src
  + test
    + java
      + apis                      Package consisting of all actions in Pet Store API
          Pet                     API calls/user actions (Serenity steps) for Pet endpoint
          Store                   API calls/user actions (Serenity steps) for Store endpoint
          User                    API calls/user actions (Serenity steps) for User endpoint
      + model                     Domain model package for generating json response
          Category                Contains data model for Category json object
          PetModel                Contains data model for Pet json response
          StoreModel              Contains data model for Store json response
          Tags                    Contains data model for Tags json object
          UserModel               Contains data model for User json response
      + stepDefinitions           Package contains step definitions
          PetStepDefinition       Step definitions for Pet endpoint feature
          StoreStepDefinition     Step definitions for Store endpoint feature
          UserStepDefinition      Step definitions for User endpoint feature
      + TestRunner                Runner class to execute/invoke feature files
    + resources
      + features                  Package contains feature files
          PetEndpoint.feature     Contains scenarios for Pet endpoint
          StoreEndpoint.feature   Contains scenarios for Store endpoint
          UserEndpoint.feature    Contains scenarios for User endpoint
      + testdata                  Package contains test data for the framework
          testImage.png
      gitlab-ci.yml               Gitlab-CI configuration file for execution and report generation in Gitlab
      pom.xml                     Maven dependency management

```
## Test Execution

Test execution can be done in 2 ways - Local and Gitlab

### Executing the tests in Gitlab

In Gitlab, we can execute the test cases manually by running the pipeline in the path `CI/CD -> Pipelines -> Run Pipeline -> Run Pipeline`.

#### Gitlab UI report

Once the pipeline is executed, we can download the report from the pipeline artifact named `"Download test:archive artifact"`. Also, the Serenity html report will be published, and it can be found in https://raghunath-dhakshnamoorthy.gitlab.io/petstore-api-automation/.

#### Job scheduled in Gitlab

Additionally, I have scheduled this job in Gitlab pipeline to execute the test cases on 1st day of every month.

### Executing the tests locally

Clone the repository from Gitlab using https://gitlab.com/Raghunath-Dhakshnamoorthy/petstore-api-automation.git to download the project in any IDE.
 
Then, run `mvn clean install` from the command line to execute all the feature files.

Once the test execution is completed, the test report will be available in `target/site/serenity/index.html`.

## Writing new automation tests

Once the project is cloned to the local, we can write new test cases or make changes to the existing ones by performing below actions.

For adding new scenarios to the existing feature, we need to make changes in the feature file and its corresponding step definition file.

For adding a new feature(file), we need to add below files in the corresponding package as per the [Project Structure](#project-structure) stated above

```
New feature file
  + StepDefinition class
     + API Steps class
        + Model class (if needed)
``` 