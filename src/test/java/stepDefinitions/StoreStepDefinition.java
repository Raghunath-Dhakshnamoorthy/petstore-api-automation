package stepDefinitions;

import apis.Store;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import model.StoreModel;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class StoreStepDefinition {
    @Steps
    Store store;

    @When("I place an order with order {int} and {int}")
    public void iPlaceAnOrderWithOrderIdAndPetId(final int id, final int petId) {
        final Response orderResponse = store.placeAnOrder(id, petId);
        Assert.assertEquals("Place new order failed with status code", 200, orderResponse.getStatusCode());
        Assert.assertEquals("Couldn't create Store with id: " + id, id, orderResponse.as(StoreModel.class).getId());
    }

    @Then("I find the purchase order with {int}")
    public void iFindThePurchaseOrderWithId(final int id) {
        store.findOrderById(id);
    }

    @When("I delete an order with {int}, I should get {int}")
    public void iDeleteAnOrderWithIdIShouldGetStatusCode(final int id, final int statusCode) {
        final Response deleteResponse = store.deleteOrder(id);
        Assert.assertEquals("Delete order failed with status code", statusCode, deleteResponse.getStatusCode());
    }

    @When("I find the pet inventories by status")
    public void iFindThePetInventoriesByStatus() {
        store.getPetInventories();
    }
}
