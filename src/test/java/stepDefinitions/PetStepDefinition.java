package stepDefinitions;

import apis.Pet;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import model.PetModel;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class PetStepDefinition {

    @Steps
    Pet pet;

    @When("I add a new pet to the store with {int}")
    public void iAddANewPetToTheStoreWithId(final int id) {
        final Response postResponse = pet.addNewPet(id);
        Assert.assertEquals("Add new pet failed with status code", 200, postResponse.getStatusCode());
        Assert.assertEquals("Couldn't create Pet with id: " + id, id, postResponse.as(PetModel.class).getId());
    }

    @Then("I should find the same pet with {int} in the store")
    public void iShouldFindTheSamePetWithIdInTheStore(final int id) {
        final Response getResponse = pet.findPetById(id);
        Assert.assertEquals("Find pet failed with status code", 200, getResponse.getStatusCode());
        Assert.assertEquals("Couldn't find Pet with id: " + id, id, getResponse.as(PetModel.class).getId());
    }

    @And("when I update the pet {int} with status {string}")
    public void whenIUpdateThePetIdWithStatus(final int id, final String status) {
        final PetModel petModel = pet.updatePet(id, status);
        Assert.assertEquals("Couldn't update Pet with id: " + id, id, petModel.getId());
        Assert.assertEquals("Failed status update: " + status, status, petModel.getStatus());
    }

    @Then("I should find the pet {int} with updated status {string}")
    public void iShouldFindThePetIdWithUpdated(final int id, final String status) {
        final Response getResponse = pet.findPetById(id);
        final PetModel petModel = getResponse.as(PetModel.class);
        Assert.assertEquals("Find pet failed with status code", 200, getResponse.getStatusCode());
        Assert.assertEquals("Couldn't find Pet with id: " + id, id, petModel.getId());
        Assert.assertEquals("Couldn't find updated pet status: " + status, status, petModel.getStatus());
    }

    @When("I update the pet {int} with name {string} and status {string}")
    public void iUpdateThePetIdWithAnd(final int id, final String name, final String status) {
        pet.updatePetWithFormData(id, name, status);
    }

    @Then("I should find the pet {int} with updated name {string} and status {string}")
    public void iShouldFindThePetIdWithUpdatedNameAndStatus(final int id, final String name, final String status) {
        final Response getResponse = pet.findPetById(id);
        final PetModel petModel = getResponse.as(PetModel.class);
        Assert.assertEquals("Find pet failed with status code", 200, getResponse.getStatusCode());
        Assert.assertEquals("Couldn't find Pet with id: " + id, id, petModel.getId());
        Assert.assertEquals("Couldn't find updated pet name: " + name, name, petModel.getName());
        Assert.assertEquals("Couldn't find updated pet status: " + status, status, petModel.getStatus());
    }

    @When("I search for pets with status {string}")
    public void iSearchForPetsWith(final String status) {
        Serenity.setSessionVariable("findPetsByStatus").to(pet.findPetByStatus(status));
    }

    @Then("I should get the status code {int}")
    public void iShouldGetTheStatusCodeStatusCode(final int statusCode) {
        final Response getResponse = Serenity.sessionVariableCalled("findPetsByStatus");
        Assert.assertEquals("Find pets failed with status code", statusCode, getResponse.getStatusCode());
    }

    @When("I delete a pet with {int}, I should get {int}")
    public void iDeleteAPetWithIdIShouldGetStatusCode(final int id, final int statusCode) {
        final Response deleteResponse = pet.deletePet(id);
        Assert.assertEquals("Delete pet failed with status code", statusCode, deleteResponse.getStatusCode());
    }

    @When("I upload image for pet with {int}, I should get {int}")
    public void iUploadImageForPetWithId(final int id, final int statusCode) {
        final Response uploadImageResponse = pet.uploadImage(id);
        Assert.assertEquals("Upload image failed with status code", statusCode, uploadImageResponse.getStatusCode());
    }
}
