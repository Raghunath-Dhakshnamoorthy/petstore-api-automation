package stepDefinitions;

import apis.User;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import model.UserModel;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class UserStepDefinition {
    @Steps
    User user;

    @When("I add a new user to the store with {int},username {string} and firstname {string}")
    public void iAddANewUserToTheStoreWithIdAndUserName(final int userId, final String userName, final String firstName) {
        final Response createResponse = user.createUser(userId, userName, firstName);
        Assert.assertEquals("Add new user failed with status code", 200, createResponse.getStatusCode());
    }

    @Then("I should find the user with user name {string}")
    public void iShouldFindTheUserWithUserName(final String userName) {
        final Response getResponse = user.getUserByUserName(userName);
        Assert.assertEquals("Find user failed with status code", 200, getResponse.getStatusCode());
        Assert.assertEquals("Couldn't find User with user name: " + userName, userName, getResponse.as(UserModel.class).getUsername());
    }

    @And("when I update the user {string} of {int} with different firstname {string}")
    public void whenIUpdateTheUserWithNewUserIdNewId(final String userName, final int userId, final String newFirstName) {
        user.updateUser(userId, userName, newFirstName);
    }

    @Then("I should find the user {string} with updated firstname {string}")
    public void iShouldFindTheUserWithUpdatedUserIdNewId(final String userName, final String newFirstName) {
        final Response getResponse = user.getUserByUserName(userName);
        final UserModel userModel = getResponse.as(UserModel.class);
        Assert.assertEquals("Find user failed with status code", 200, getResponse.getStatusCode());
        Assert.assertEquals("Couldn't find User with user name: " + userName, userName, userModel.getUsername());
        Assert.assertEquals("Couldn't find updated firstname: " + newFirstName, newFirstName, userModel.getFirstName());
    }

    @When("I delete an user with user name {string}, I should get {int}")
    public void iDeleteAnUserWithUserNameIShouldGetStatusCode(final String userName, final int statusCode) {
        final Response deleteResponse = user.deleteUser(userName);
        Assert.assertEquals("Delete user failed with status code", statusCode, deleteResponse.getStatusCode());
    }

    @Given("I login with username {string} and password {string}")
    public void iLoginWithUsernameAndPassword(final String userName, final String passWord) {
        Serenity.setSessionVariable("loginResponse").to(user.loginUser(userName, passWord));
    }

    @Then("I should login successfully with status code {int}")
    public void iShouldLoginSuccessfullyWithStatusCode(final int statusCode) {
        final Response loginResponse = Serenity.sessionVariableCalled("loginResponse");
        Assert.assertEquals("Login user failed with status code", statusCode, loginResponse.getStatusCode());
    }

    @And("also I should also be able to logout successfully")
    public void alsoIShouldAlsoBeAbleToLogoutSuccessfully() {
        user.logoutUser();
    }

    @Given("I should be able to create user with id {int}, username {string} and firstname {string} successfully with array")
    public void iShouldBeAbleToCreateUserWithIdAndNameSuccessfullyWithArray(final int userId, final String userName, final String firstName) {
        user.createWithArray(userId, userName, firstName);
    }

    @And("also I should be able to create user with id {int}, username {string} and firstname {string} successfully with list")
    public void alsoIShouldBeAbleToCreateUserWithIdAndNameSuccessfullyWithList(final int userId, final String userName, final String firstName) {
        user.createWithList(userId, userName, firstName);
    }
}
