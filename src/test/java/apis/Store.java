package apis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import model.StoreModel;
import net.thucydides.core.annotations.Step;

import java.time.Instant;

@Slf4j
public class Store {

    final String storeUrl = "https://petstore.swagger.io/v2/store";

    /**
     * Method to retrieve order by id
     *
     * @param orderId
     *         order id
     */
    @Step
    public void findOrderById(final int orderId) {
        RestAssured.given().header("Accept", "application/json")
                .when().log().all()
                .get(storeUrl + "/order/" + orderId)
                .then().statusCode(200);
    }

    /**
     * Method to place a new order
     *
     * @param orderId
     *         order id
     * @param petId
     *         pet id for which order is placed
     * @return {@link Response}
     */
    @Step
    public Response placeAnOrder(final int orderId, final int petId) {
        return RestAssured.given()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(setStoreData(orderId, petId))
                .when().log().all()
                .post(storeUrl + "/order")
                .then().extract().response();
    }

    /**
     * Method to retrieve inventory details
     */
    @Step
    public void getPetInventories() {
        RestAssured.given().header("Accept", "application/json")
                .when().log().all()
                .get(storeUrl + "/inventory")
                .then().statusCode(200);
    }

    /**
     * Method to delete the order
     *
     * @param orderId
     *         order id
     * @return {@link Response}
     */
    @Step
    public Response deleteOrder(final int orderId) {
        return RestAssured.given().header("Accept", "application/json")
                .when().log().all()
                .delete((storeUrl + "/order/" + orderId))
                .then().extract().response();
    }

    /**
     * Method to generate the store data
     *
     * @param storeId
     *         store id
     * @param petId
     *         pet id
     * @return {@link String} store json as String
     */
    private String setStoreData(final int storeId, final int petId) {
        final StoreModel storeModel = new StoreModel();
        storeModel.setId(storeId);
        storeModel.setPetId(petId);
        storeModel.setQuantity(1);
        storeModel.setShipDate(Instant.now().toString());
        storeModel.setStatus("placed");
        storeModel.setComplete(true);
        final Gson gsonBuilder = new GsonBuilder().create();
        return gsonBuilder.toJson(storeModel);
    }
}
