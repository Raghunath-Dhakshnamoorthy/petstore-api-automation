package apis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import model.Category;
import model.PetModel;
import model.Tags;
import net.thucydides.core.annotations.Step;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Pet {

    final String petUrl = "https://petstore.swagger.io/v2/pet";

    /**
     * Method to retrieve pet by id
     *
     * @param petId
     *         pet id to be retrieved
     * @return {@link Response}
     */
    @Step
    public Response findPetById(final int petId) {
        return RestAssured.given().header("Accept", "application/json")
                .when().log().all()
                .get(petUrl + "/" + petId)
                .then().extract().response();
    }

    /**
     * Method to create new pet
     *
     * @param petId
     *         pet details to be created with given pet id
     * @return {@link Response}
     */
    @Step
    public Response addNewPet(final int petId) {
        return RestAssured.given()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(setPetData(petId, "available"))
                .when().log().all()
                .post(petUrl)
                .then().extract().response();
    }

    /**
     * Method to update pet with given details
     *
     * @param petId
     *         pet id for which details needs to be updated
     * @param status
     *         updated pet status
     * @return {@link PetModel}
     */
    @Step
    public PetModel updatePet(final int petId, final String status) {
        return RestAssured.given()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(setPetData(petId, status))
                .when().log().all()
                .put(petUrl)
                .then().statusCode(200).extract().as(PetModel.class);
    }

    /**
     * Method to find pet details by status
     *
     * @param status
     *         status to check the pet store
     * @return {@link Response}
     */
    @Step
    public Response findPetByStatus(final String status) {
        return RestAssured.given().header("Accept", "application/json")
                .queryParam("status", status)
                .when().log().all()
                .get(petUrl + "/findByStatus")
                .then().extract().response();
    }

    /**
     * Method to update pet with form data
     *
     * @param petId
     *         pet id for which details needs to be updated
     * @param updatedName
     *         pet name to be updated
     * @param updatedStatus
     *         pet status to be updated
     */
    @Step
    public void updatePetWithFormData(final int petId, final String updatedName, final String updatedStatus) {
        final String body = String.format("name=%s&status=%s", updatedName, updatedStatus);
        RestAssured.given()
                .config(RestAssured.config()
                        .encoderConfig(EncoderConfig.encoderConfig()
                                .encodeContentTypeAs("x-www-form-urlencoded",
                                        ContentType.URLENC)))
                .baseUri(petUrl)
                .header("Accept", "application/json")
                .contentType("application/x-www-form-urlencoded; charset=UTF-8")
                .body(body)
                .when().log().all()
                .post("/" + petId)
                .then().statusCode(200);
    }

    /**
     * Method to delete the pet
     *
     * @param petId
     *         pet id to be deleted
     * @return {@link Response}
     */
    @Step
    public Response deletePet(final int petId) {
        return RestAssured.given().header("Accept", "application/json")
                .when().log().all()
                .delete((petUrl + "/" + petId))
                .then().extract().response();
    }

    /**
     * Method to upload image to a pet
     *
     * @param petId
     *         pet id to upload image
     * @return {@link Response}
     */
    @Step
    public Response uploadImage(final int petId) {
        File file = new File("src/test/resources/testdata/testImage.png");
        return RestAssured.given()
                .header("Accept", "application/json")
                .header("Content-Type", "multipart/form-data")
                .multiPart("file", file)
                .when().log().all()
                .post(petUrl + "/" + petId + "/uploadImage")
                .then().extract().response();
    }

    /**
     * Method to generate the pet data
     *
     * @param petId
     *         pet id
     * @param status
     *         pet status
     * @return {@link String} pet json as String
     */
    private String setPetData(final int petId, final String status) {
        final List<Tags> tagsList = new ArrayList<>();
        tagsList.add(new Tags(0, "String"));
        final List<String> photoUrls = new ArrayList<>();
        photoUrls.add("String");
        final PetModel petModel = new PetModel();
        petModel.setId(petId);
        petModel.setName("doggie");
        petModel.setStatus(status);
        petModel.setTags(tagsList);
        petModel.setCategory(new Category(0, "String"));
        petModel.setPhotoUrls(photoUrls);
        final Gson gsonBuilder = new GsonBuilder().create();
        return gsonBuilder.toJson(petModel);
    }
}
