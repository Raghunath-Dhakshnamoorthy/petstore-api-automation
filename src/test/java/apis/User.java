package apis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import model.UserModel;
import net.thucydides.core.annotations.Step;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class User {

    final String userUrl = "https://petstore.swagger.io/v2/user";

    /**
     * Method to retrieve user by user name
     *
     * @param userName
     *         user name
     * @return {@link Response}
     */
    @Step
    public Response getUserByUserName(final String userName) {
        return RestAssured.given().header("Accept", "application/json")
                .when().log().all()
                .get(userUrl + "/" + userName)
                .then().extract().response();
    }

    /**
     * Method to create array of users
     *
     * @param userId
     *         user id
     * @param userName
     *         user name
     * @param firstName
     *         first name
     */
    @Step
    public void createWithArray(final int userId, final String userName, final String firstName) {
        RestAssured.given()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(setUserListData(userId, userName, firstName))
                .when().log().all()
                .post(userUrl + "/createWithArray")
                .then().statusCode(200);
    }

    /**
     * Method to create list of users
     *
     * @param userId
     *         user id
     * @param userName
     *         user name
     * @param firstName
     *         first name
     */
    @Step
    public void createWithList(final int userId, final String userName, final String firstName) {
        RestAssured.given()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(setUserListData(userId, userName, firstName))
                .when().log().all()
                .post(userUrl + "/createWithList")
                .then().statusCode(200);
    }

    /**
     * Method to update user with given details
     *
     * @param userId
     *         user id
     * @param userName
     *         user name
     * @param firstName
     *         first name
     */
    @Step
    public void updateUser(final int userId, final String userName, final String firstName) {
        RestAssured.given()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(setUserData(userId, userName, firstName))
                .when().log().all()
                .put(userUrl + "/" + userName)
                .then().statusCode(200);
    }

    /**
     * Method to login user
     *
     * @param userName
     *         user name
     * @param passWord
     *         password
     * @return {@link Response}
     */
    @Step
    public Response loginUser(final String userName, final String passWord) {
        return RestAssured.given().header("Accept", "application/json")
                .queryParam("username", userName)
                .queryParam("password", passWord)
                .when().log().all()
                .get(userUrl + "/login")
                .then().extract().response();
    }

    /**
     * Method to logout user
     */
    @Step
    public void logoutUser() {
        RestAssured.given().header("Accept", "application/json")
                .when().log().all()
                .get(userUrl + "/logout")
                .then().statusCode(200);
    }

    /**
     * Method to delete user
     *
     * @param userName
     *         user name
     * @return {@link Response}
     */
    @Step
    public Response deleteUser(final String userName) {
        return RestAssured.given().header("Accept", "application/json")
                .when().log().all()
                .delete((userUrl + "/" + userName))
                .then().extract().response();
    }

    /**
     * Method to create user with given details
     *
     * @param userId
     *         user id
     * @param userName
     *         user name
     * @param firstName
     *         first name
     * @return {@link Response}
     */
    @Step
    public Response createUser(final int userId, final String userName, final String firstName) {
        return RestAssured.given()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(setUserData(userId, userName, firstName))
                .when().log().all()
                .post(userUrl)
                .then().extract().response();
    }

    /**
     * Method to generate user list data
     *
     * @param userId
     *         user id
     * @param userName
     *         user name
     * @param firstName
     *         first name
     * @return {@link String} user list json as String
     */
    private String setUserListData(final int userId, final String userName, final String firstName) {
        List<UserModel> userModels = new ArrayList<>();
        userModels.add(setUserModel(userId, userName, firstName));
        final Gson gsonBuilder = new GsonBuilder().create();
        return gsonBuilder.toJson(userModels);
    }

    /**
     * Method to generate user data
     *
     * @param userId
     *         user id
     * @param userName
     *         user name
     * @param firstName
     *         first name
     * @return {@link String} user json as String
     */
    private String setUserData(final int userId, final String userName, final String firstName) {
        final Gson gsonBuilder = new GsonBuilder().create();
        return gsonBuilder.toJson(setUserModel(userId, userName, firstName));
    }

    /**
     * Method to generate user data with UserModel class
     *
     * @param userId
     *         user id
     * @param userName
     *         user name
     * @param firstName
     *         first name
     * @return {@link UserModel}
     */
    private UserModel setUserModel(final int userId, final String userName, final String firstName) {
        final UserModel userModel = new UserModel();
        userModel.setId(userId);
        userModel.setUsername(userName);
        userModel.setFirstName(firstName);
        userModel.setLastName("lastName");
        userModel.setEmail("email");
        userModel.setPassword("passWord");
        userModel.setPhone("phone");
        userModel.setUserStatus(1);
        return userModel;
    }
}
