Feature: Test the operations for Pet endpoint

  Scenario Outline: Find pets by status
    When I search for pets with status "<status>"
    Then I should get the status code <statusCode>
    Examples:
      | status    | statusCode |
      | available | 200        |
      | pending   | 200        |
      | sold      | 200        |
