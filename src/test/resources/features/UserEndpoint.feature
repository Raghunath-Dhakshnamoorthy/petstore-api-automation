Feature: Test the operations for User endpoint

  Scenario Outline: Login and logout
    Given I login with username "<userName>" and password "<passWord>"
    Then I should login successfully with status code 200
    And also I should also be able to logout successfully
    Examples:
      | userName    | passWord     |
      | testPetUser | testPassword |

  Scenario: Create user with array and list
    Given I should be able to create user with id 112, username "testArrayUser" and firstname "testArray" successfully with array
    And also I should be able to create user with id 221, username "testListUser" and firstname "testList" successfully with list
